# Motion_Detected_Alarm

Python Open CV application for motion detection and voice alarm.
It requires python3 and:
numpy
opencv-python
pyttsx3
pytz
pywin32
pywin32-ctypes

once downloaded run:


> pip3 install -r requirements.txt

then run: 


> python3 motion_detector.py
